### Alias

```bash
alias ipyspark="PYSPARK_DRIVER_PYTHON=ipython pyspark"
alias activate3="source ~/.venv/bin/activate"
alias activate="source .venv/bin/activate"
alias o="open ."
alias q="exit"
alias cmt="~/.yong/cmt.sh"
alias jn="jupyter notebook"
alias chrome="open -a Google\ Chrome"
alias octave="/usr/local/octave/3.8.0/bin/octave-3.8.0"
alias zshconfig="vim ~/.zshrc"
```

##### autojump

- `brew install autojump`
- change .zshrc `plugins=(git autojump)`
- add `[[ -s ~/.autojump/etc/profile.d/autojump.sh ]] && . ~/.autojump/etc/profile.d/autojump.sh
`to .zshrc
- [reference](http://macshuo.com/?p=676)